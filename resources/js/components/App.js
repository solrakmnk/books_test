import React from 'react'
import {BrowserRouter, Route, Switch } from 'react-router-dom'
import Genres from "../pages/Genres";
import NotFound from "../pages/404"
import GenresBooks from "../pages/GenresBooks";
import Books from '../pages/Books'


const App=()=>(
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Genres}></Route>
            <Route exact path="/genre/:id/books" component={GenresBooks}></Route>
            <Route exact path="/books/:id" component={Books}></Route>
            <Route component={NotFound}/>
        </Switch>
    </BrowserRouter>
)

export default App