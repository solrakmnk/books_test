import React from 'react'
import {Link} from 'react-router-dom'

class GenreCard extends React.Component {
    render() {
        const { id,name} = this.props
    return(
        <>
            <div className="card-body">
                <Link to={`/genre/${id}/books`}>{name}</Link>
            </div>
        </>
    )}
}


export default GenreCard
