import React from 'react'
import {Link} from 'react-router-dom'

class BookCard extends React.Component {
    render() {
        const { id,title} = this.props
    return(
        <>
            <div className="card-body">
                <h5 className="card-title">{title}</h5>
                <Link to={`/books/${id}`}>Ver</Link>
            </div>
        </>
    )}
}


export default BookCard
