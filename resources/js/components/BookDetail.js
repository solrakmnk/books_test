import React from 'react'
import BookCard from './BookCard'
import {Link} from "react-router-dom";

const BookDetail = ({id, title, author,genre, isbn, img_url,goodreads_rating,amazon_rating}) => (
    <div className="container">
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="media">
                        <img src={img_url} className="mr-3"/>
                            <div className="media-body">
                                <h5 className="mt-0">{title}</h5>
                                <p>Author: {author}</p>
                                <p>ISBN: {isbn}</p>
                                <p>Genero: {genre}</p>
                                <p>Rating Amazon: {amazon_rating}</p>
                                <p>Rating Good Reads: {goodreads_rating}</p>
                            </div>
                    </div>
                </div>
                <div>
                    <Link to={`/`}>Volver al inicio</Link>
                </div>
            </div>
        </div>
    </div>
)


export default BookDetail