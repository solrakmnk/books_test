import React from 'react'
import GenreCard from './GenreCard'

const GenreList=({genres})=> (
    <div className="container">
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-header">Generos</div>
                    {genres.map((genre)=>(
                        <GenreCard
                            key={genre.id}
                            {...genre}
                        />
                    ))}
                </div>
            </div>
        </div>
    </div>
)


export default GenreList