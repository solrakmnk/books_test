import React from 'react'
import BookCard from './BookCard'

const BookList=({books})=> (
    <div className="container">
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-header">Libros</div>
                    {books.map((book)=>(
                        <BookCard
                            key={book.id}
                            {...book}
                        />
                    ))}
                </div>
            </div>
        </div>
    </div>
)


export default BookList