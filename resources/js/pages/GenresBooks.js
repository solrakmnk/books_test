import React from 'react'
import BookList from '../components/BookList'
import Loading from '../components/Loading'
import FatalError from './500'
import url from '../config'

class GenresBooks extends React.Component {
    state={
        data:[],
        loading: true,
        error: null
    }

    async componentDidMount(){
        await this.fetchGenres()
    }

    fetchGenres = async() =>{
        try{
            const params=this.props.match.params

            let res = await fetch(`${url}/api/genres/${params.id}/books`)
            let data = await res.json()
            this.setState({
                data:data.data,
                loading:false
            })
        } catch(error){
            this.setState({
                loading:false,
                error
            })
        }
    }

    render() {
        if(this.state.loading){
            return <Loading/>
        }
        if(this.state.error){
            return <FatalError/>
        }
        return(
            <React.Fragment>
                <BookList books={this.state.data}></BookList>
            </React.Fragment>
        )
    }
}

export default GenresBooks