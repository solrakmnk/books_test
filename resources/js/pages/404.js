import React from 'react'

const NotFound = () => (
    <div className="text-center">
        <h1>Error: 404 Page Not Found</h1>
    </div>
)

export default NotFound