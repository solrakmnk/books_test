import React from 'react'
import BookDetail from '../components/BookDetail'
import Loading from '../components/Loading'
import FatalError from './500'
import url from '../config'

class Books extends React.Component {
    state={
        data:[],
        loading: true,
        error: null
    }

    async componentDidMount(){
        await this.fetchGenres()
    }

    fetchGenres = async() =>{
        try{
            const params=this.props.match.params

            let res = await fetch(`${url}/api/books/${params.id}`)
            let data = await res.json()
            this.setState({
                data:data.data,
                loading:false
            })
        } catch(error){
            this.setState({
                loading:false,
                error
            })
        }
    }

    render() {
        if(this.state.loading){
            return <Loading/>
        }
        if(this.state.error){
            return <FatalError/>
        }
        return(
            <React.Fragment>
                <BookDetail {...this.state.data}></BookDetail>
            </React.Fragment>
        )
    }
}

export default Books