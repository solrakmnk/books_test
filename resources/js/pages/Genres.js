import React from 'react'
import GenreList from '../components/GenreList'
import Loading from '../components/Loading'
import FatalError from './500'
import url from '../config'

class Genres extends React.Component {
    state={
        data:[],
        loading: true,
        error: null
    }

    async componentDidMount(){
        await this.fetchGenres()
    }

    fetchGenres = async() =>{
        try{
            let res = await fetch(`${url}/api/genres`)
            let data = await res.json()
            this.setState({
                data:data.data,
                loading:false
            })
        } catch(error){
            this.setState({
                loading:false,
                error
            })
        }
    }

    render() {
        if(this.state.loading){
            return <Loading/>
        }
        if(this.state.error){
            return <FatalError/>
        }
        return(
            <React.Fragment>
                <GenreList genres={this.state.data}></GenreList>
            </React.Fragment>
        )
    }
}

export default Genres