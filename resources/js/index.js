import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'

require ('bootstrap')

import 'bootstrap/dist/css/bootstrap.css'

const container = document.getElementById('app')

ReactDOM.render(<App/>, container)