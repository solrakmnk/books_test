<?php

namespace Tests\Feature\Models;

use App\Models\Genre;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;


class GenreTest extends TestCase
{
    use DatabaseMigrations;

    public function testCreateGenre()
    {
        $genre = factory(Genre::class)->create();
        $this->assertDatabaseHas('genres', [
            'id'   => $genre->id,
            'name' => $genre->name
        ]);
    }

    public function testCanNotCreateDuplicatedGenre()
    {
        Genre::create(['name' => 'Thriller']);
        try {
            Genre::create(['name' => 'Thriller']);
        } catch (QueryException $e) {
            $this->assertEquals(23000,$e->getCode());
        }
        $genres=Genre::all();
        $this->assertEquals(1,$genres->count());

    }
}
