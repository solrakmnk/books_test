<?php

namespace Tests\Feature\Models;

use App\Models\Author;
use App\Models\Book;
use App\Models\Genre;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase
{
    use DatabaseMigrations;
    public function testCreateBook()
    {
        $book=new Book(['title'=>'It','isbn'=>'123']);
        $author=factory(Author::class)->create();
        $genre=factory(Genre::class)->create();
        $book->author()->associate($author);
        $book->genre()->associate($genre);
        $book->save();
        $this->assertDatabaseHas('books', [
            'id'   => $book->id,
            'title' => $book->title,
            'isbn' => '123'
        ]);
    }
}
