<?php

namespace Tests\Feature\Models;

use App\Models\Author;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;


class AuthorTest extends TestCase
{
    use DatabaseMigrations;
    public function testCreateAuthor()
    {
        $author=factory(Author::class)->create();
        $this->assertDatabaseHas('authors', [
            'id'   => $author->id,
            'name' => $author->name
        ]);
    }
}
