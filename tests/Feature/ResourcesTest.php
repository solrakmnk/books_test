<?php

namespace Tests\Feature;

use App\Models\Genre;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResourcesTest extends TestCase
{
    public function testGetResources()
    {
        $genre = "thriller";
        $response = $this->get("/resources/{$genre}");

        $genres = Genre::all();
        $this->assertEquals(10, $genres->count());


        $response->assertStatus(200);
    }

    public function testSuccessResponse()
    {
        $genre = "thriller";
        $response = $this->get("/resources/{$genre}");
        $response->assertJsonStructure(
            [
                'data',
                'meta'
            ]
        );
    }

    public function testErrorResponse()
    {

    }
}
