<?php

namespace App\Transformers;

use App\Models\Author;
use League\Fractal\TransformerAbstract;

class AuthorTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Author $author)
    {
        return [
            'name'  => $author->name,
            'links' => [
                [
                    'rel'  => 'self',
                    'href' => route('authors.show', $author),
                ],
                [
                    'rel' => 'author.books',
                    'href' => route('authors.books.index',$author)
                ]


            ]
        ];
    }
}
