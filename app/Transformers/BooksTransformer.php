<?php

namespace App\Transformers;

use App\Models\Book;
use League\Fractal\TransformerAbstract;

class BooksTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Book $book)
    {
        return [
            'id'               => $book->id,
            'title'            => $book->title,
            'isbn'             => $book->isbn,
            'goodreads_rating' => (float)$book->goodreads_rating,
            'amazon_rating'    => (float)$book->amazon_rating,
            'img_url'          => $book->image_url,
            'author'           => $book->author->name,
            'genre'            => $book->genre->name,
            'links'=>[
                [
                    'rel' => 'self',
                    'href' => route('books.show', $book->id)
                ],
                [
                    'rel' => 'author',
                    'href' => route('authors.show', $book->author)
                ],
                [
                    'rel' => 'genre',
                    'href' => route('genres.show', $book->genre)
                ],
            ]
        ];
    }
}
