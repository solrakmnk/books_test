<?php

namespace App\Repositories;

use App\Models\Author;
use App\Models\Book;
use App\Models\Genre;
use GuzzleHttp\Client;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class GoodReadsRespository
{

    protected $query;
    protected $client;

    public function __construct()
    {
        $this->client = $client = new Client();
        $this->baseUrl = 'https://www.goodreads.com';
        $key = config('services.goodreads_key');
        $this->query = ['key' => $key];
    }

    public function getBooksByGenre($genre)
    {
        $query = array_merge($this->query,
            [
                'q'             => $genre,
                'search[filed]' => 'genre'
            ]
        );

        $response = $this->client->request(
            'GET', $this->baseUrl . '/search/index.xml',
            ['query' => $query]
        );
        $contents = new \SimpleXMLElement($response->getBody()->getContents());
        $results = $contents->search->results->work;
        return $results;
    }

    protected function getDataById($id)
    {
        $response = $this->client->request(
            'GET', $this->baseUrl . "/book/show/{$id}.xml",
            ['query' => $this->query]
        );
        $contents = new \SimpleXMLElement($response->getBody()->getContents());
        $result = $contents->book;
        return $result;

    }

    public function saveBooksById($books,$genre_name)
    {
        try{
            $genre= Genre::firstOrCreate(['name'=>$genre_name]);
        }
        catch(QueryException $e){
            Log::info("Registro Duplicado");
        }
        foreach ($books as $book) {
            try {
                $bookData = $this->getDataById($book->best_book->id);
                $newBook= new Book([
                    'title'            => $bookData->title,
                    'isbn'             => $bookData->isbn,
                    'goodreads_rating' => $bookData->average_rating,
                    'image_url'        => $bookData->image_url
                ]);
                $authorName=$bookData->authors->author->name;
                $author= Author::firstOrCreate(['name'=>$authorName]);
                $newBook->author()->associate($author);
                $newBook->genre()->associate($genre)->save();
            } catch (QueryException $e) {
                Log::info("Registro Duplicado");
            }
        }
    }
}