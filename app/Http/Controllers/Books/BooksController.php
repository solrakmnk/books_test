<?php

namespace App\Http\Controllers\Books;

use App\Http\Controllers\apiController;
use App\Models\Book;
use Illuminate\Http\Request;

class BooksController extends apiController
{
    public function index(Request $request){
        return $this->showAll(Book::all());
    }

    public function show(Book $book){
        return $this->showOne($book);
    }
}
