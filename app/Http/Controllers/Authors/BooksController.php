<?php

namespace App\Http\Controllers\Authors;

use App\Http\Controllers\apiController;
use App\Models\Author;
use App\Models\Book;


class BooksController extends apiController
{
    public function index(Author $author){
        $collection=$author->books;
        return $this->showAll($collection);
    }
}
