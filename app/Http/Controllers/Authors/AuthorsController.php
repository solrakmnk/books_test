<?php

namespace App\Http\Controllers\Authors;

use App\Http\Controllers\apiController;
use App\Models\Author;

class AuthorsController extends apiController
{
    public function index(){
        return $this->showAll(Author::all());
    }
    public function show(Author $author){
        return $this->showOne($author);
    }
}
