<?php

namespace App\Http\Controllers\Genres;

use App\Http\Controllers\apiController;
use App\Models\Genre;


class BooksController extends apiController
{
    public function index(Genre $genre){
        $books=$genre->books;
        return $this->showAll($books);
    }
}
