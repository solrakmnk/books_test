<?php

namespace App\Http\Controllers\Genres;

use App\Http\Controllers\apiController;
use App\Models\Genre;


class GenresController extends apiController
{
    public function index(){
        return $this->showAll(Genre::all());
    }

    public function show(Genre $genre){
        return $this->showOne($genre);
    }
}
