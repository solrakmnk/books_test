<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Repositories\GoodReadsRespository;
use App\Traits\ApiResponser;

class SourcesController extends Controller
{
    use ApiResponser;
    protected $goodReadsRepository;

    public function __construct(GoodReadsRespository $goodReadsRepository)
    {
        $this->goodReadsRepository = $goodReadsRepository;
    }

    public function getSources($genre)
    {
        $books = $this->goodReadsRepository->getBooksByGenre($genre);
        if ($books->count() === 0) {
            $this->errorResponse("Genero sin resultados",200);
        }
        $this->goodReadsRepository->saveBooksById($books, $genre);
        $booksCollection=Book::all();
        return $this->showAll($booksCollection,201);
    }

}
