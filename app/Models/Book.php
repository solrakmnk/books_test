<?php

namespace App\Models;

use App\Transformers\BooksTransformer;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title','isbn','goodreads_rating','amazon_rating','image_url'];
    public $transformer = BooksTransformer::class;
    public function genre(){
        return $this->belongsTo(Genre::class);
    }

    public function author(){
        return $this->belongsTo(Author::class);
    }
}
