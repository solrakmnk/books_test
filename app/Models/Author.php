<?php

namespace App\Models;

use App\Transformers\AuthorTransformer;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public $transformer = AuthorTransformer::class;

    protected $fillable=['name'];
    public function books(){
        return $this->hasMany(Book::class);
    }

}
