<?php

namespace App\Models;

use App\Transformers\GenreTransformer;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public $transformer = GenreTransformer::class;
    protected $fillable=['name'];
    public function books(){
        return $this->hasMany(Book::class);
    }
}
