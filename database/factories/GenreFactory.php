<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use App\Models\Genre as Genre;
use Faker\Generator as Faker;

$factory->define(Genre::class, function (Faker $faker) {
    return [
        'name'=>$faker->randomElement(['Thiller','Romance','Fiction','Fantasy','Satery','Poetry'])
    ];
});
