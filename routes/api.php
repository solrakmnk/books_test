<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');


Route::get('sources/{genre}','SourcesController@getSources');

Route::resource('books','Books\BooksController')->only(['index','show']);

Route::resource('genres','Genres\GenresController')->only(['index','show']);

Route::resource('genres.books','Genres\BooksController')->only(['index']);

Route::resource('authors','Authors\AuthorsController')->only(['index','show']);

Route::resource('authors.books','Authors\BooksController')->only(['index']);

